| Activity                                         | Date         | Comment                                                                         |
|--------------------------------------------------|--------------|---------------------------------------------------------------------------------|
| initial commit                                   | 10 Apr, 2023 | created empty project with needed packages                                      |
| added entities and csv files                     | 17 Apr, 2023 | added entity classes with minimal possible state                                |
| updated .gitignore                               | 17 Apr, 2023 | added target folder to .gitignore                                               |
| updated csv files                                | 17 Apr, 2023 | updated the structure of csv files                                              |
| added csv manager                                | 27 Apr, 2023 | the class that controls the operating with csv files                            |
| fixed write method of CsvManager.java            | 01 May, 2023 | it did writes data in correct format, i fixed it                                |
| added wrapper                                    | 16 May, 2023 | added the wrapping data to entities and fixed some bugs                         |
| added router, validators and console manager     | 17 May, 2023 | Created the classes for working with validation routing and console             |
| router updated, got better                       | 18 May, 2023 | become more abstract                                                            |
| added executable method to controller            | 19 May, 2023 | wrapper method that wraps logic in this method with try catch                   |
| added search method to entities                  | 22 May, 2023 | added the search method for searching the data from csv files                   |
| updated the logic of entities and added services | 23 May, 2023 | entities refactored and become more abstract                                    |
| added all types of furniture                     | 23 May, 2023 | there is only bed category, after that i added bookcase, chair, sofa categories |
| added custom method for searching                | 23 May, 2023 | added custom method for searching custom columns and removed unused methods     |
| minimized filter method in SearchService         | 24 May, 2023 | no comment                                                                      |
| unified controllers and entities                 | 26 May, 2023 | cleaned code repeating                                                          |
| added tests                                      | 27 May, 2023 | added tests for SearchService and Entities                                      |
| unified furniture services                       | 29 May, 2023 | cleaned code repeating                                                          |
