import org.dorsone.app.constants.FileNames;
import org.dorsone.app.dto.*;
import org.dorsone.app.entities.Bed;
import org.dorsone.app.entities.Bookcase;
import org.dorsone.app.entities.Chair;
import org.dorsone.app.entities.Sofa;
import org.dorsone.app.managers.CsvManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class FurnitureTest {
    @Test
    public void bedGetAllTest() throws FileNotFoundException {
        ArrayList<? extends FurnitureDTO> beds = Bed.parseToArrayList();
        FileReader reader = new FileReader(FileNames.bed.getPath());

        try {
            ArrayList<Map<String, String>> data = CsvManager.parse(reader);

            BedDTO dto1 = (BedDTO) beds.get(0);
            BedDTO dto2 = (BedDTO) (new BedDTO(data)).toArrayList().get(0);

            Assertions.assertEquals(dto1.price, dto2.price);
            Assertions.assertEquals(dto1.width, dto2.width);
            Assertions.assertEquals(dto1.depth, dto2.depth);
            Assertions.assertEquals(dto1.height, dto2.height);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void bookcaseGetAllTest() throws FileNotFoundException {
        ArrayList<? extends FurnitureDTO> bookcases = Bookcase.parseToArrayList();
        FileReader reader = new FileReader(FileNames.bookcase.getPath());

        try {
            ArrayList<Map<String, String>> data = CsvManager.parse(reader);

            BookcaseDTO dto1 = (BookcaseDTO) bookcases.get(0);
            BookcaseDTO dto2 = (BookcaseDTO) (new BookcaseDTO(data)).toArrayList().get(0);

            Assertions.assertEquals(dto1.price, dto2.price);
            Assertions.assertEquals(dto1.width, dto2.width);
            Assertions.assertEquals(dto1.depth, dto2.depth);
            Assertions.assertEquals(dto1.height, dto2.height);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void chairGetAllTest() throws FileNotFoundException {
        ArrayList<? extends FurnitureDTO> chairs = Chair.parseToArrayList();
        FileReader reader = new FileReader(FileNames.chair.getPath());

        try {
            ArrayList<Map<String, String>> data = CsvManager.parse(reader);

            ChairDTO dto1 = (ChairDTO) chairs.get(0);
            ChairDTO dto2 = (ChairDTO) (new ChairDTO(data)).toArrayList().get(0);

            Assertions.assertEquals(dto1.price, dto2.price);
            Assertions.assertEquals(dto1.width, dto2.width);
            Assertions.assertEquals(dto1.depth, dto2.depth);
            Assertions.assertEquals(dto1.height, dto2.height);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void sofaGetAllTest() throws FileNotFoundException {
        ArrayList<? extends FurnitureDTO> sofas = Sofa.parseToArrayList();
        FileReader reader = new FileReader(FileNames.sofa.getPath());

        try {
            ArrayList<Map<String, String>> data = CsvManager.parse(reader);

            SofaDTO dto1 = (SofaDTO) sofas.get(0);
            SofaDTO dto2 = (SofaDTO) (new SofaDTO(data)).toArrayList().get(0);

            Assertions.assertEquals(dto1.price, dto2.price);
            Assertions.assertEquals(dto1.width, dto2.width);
            Assertions.assertEquals(dto1.depth, dto2.depth);
            Assertions.assertEquals(dto1.height, dto2.height);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
