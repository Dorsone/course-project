
import org.dorsone.app.dto.BedDTO;
import org.dorsone.app.dto.FurnitureDTO;
import org.dorsone.app.entities.Bed;
import org.dorsone.app.exceptions.ValidationErrorException;
import org.dorsone.app.managers.ConsoleManager;
import org.dorsone.app.services.SearchService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class SearchServiceTest {

    @Test
    public void getByTest() {
        ArrayList<? extends FurnitureDTO> beds = Bed.parseToArrayList();
        SearchService service = new SearchService(beds, new ConsoleManager());

        try {
            List<? extends FurnitureDTO> filteredBed = service.getBy("price", "12000");
            BedDTO bed = (BedDTO) filteredBed.get(0);

            Assertions.assertEquals(bed.height, 45.5);
            Assertions.assertEquals(bed.width, 59.5);
            Assertions.assertEquals(bed.price, 12000);
            Assertions.assertEquals(bed.depth, 60);
            Assertions.assertEquals(bed.numOfPeopleForSleep, 2);

        } catch (ValidationErrorException e) {
            throw new RuntimeException(e);
        }
    }
}
