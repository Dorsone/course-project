package org.dorsone.app.services;

import org.dorsone.app.exceptions.ValidationErrorException;
import org.dorsone.app.managers.ConsoleManager;
import org.dorsone.app.validators.Validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public abstract class FurnitureService extends Service {
    protected LinkedList<String> commands;
    protected ArrayList<Validator> validators;

    public FurnitureService(ConsoleManager consoleManager, LinkedList<String> commands, ArrayList<Validator> validators) {
        super(consoleManager);
        this.commands = commands;
        this.validators = validators;
    }

    public HashMap<String, String> getCommandValue() throws ValidationErrorException {
        this.consoleManager.input("\nChoose command " + this.commands + ": ")
                .backInsteadOfExit()
                .addValidators(this.validators);

        return this.getCommandValueOnly();
    }
}
