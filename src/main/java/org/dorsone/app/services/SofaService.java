package org.dorsone.app.services;

import org.dorsone.app.entities.Sofa;
import org.dorsone.app.exceptions.ValidationErrorException;
import org.dorsone.app.managers.ConsoleManager;
import org.dorsone.app.validators.ValidateSofaCommand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class SofaService extends FurnitureService {
    public SofaService(ConsoleManager consoleManager) {
        super(consoleManager, Sofa.getCommands(), new ArrayList<>(Collections.singleton(new ValidateSofaCommand())));
    }

    public HashMap<String, String> getCommandValue() throws ValidationErrorException {
        this.consoleManager.input("\nChoose command " + Sofa.getCommands() + ": ")
                .backInsteadOfExit()
                .addValidator(new ValidateSofaCommand());

        return this.getCommandValueOnly();
    }
}
