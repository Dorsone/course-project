package org.dorsone.app.services;

import org.dorsone.app.exceptions.ValidationErrorException;
import org.dorsone.app.managers.ConsoleManager;

import java.util.HashMap;

abstract public class Service {
    protected final ConsoleManager consoleManager;

    public Service(ConsoleManager consoleManager) {
        this.consoleManager = consoleManager;
    }

    protected HashMap<String, String> getCommandValueOnly() throws ValidationErrorException {
        HashMap<String, String> result = new HashMap<>();

        String command = this.consoleManager.getInputResult();

        String value = "";
        if (!command.equals("getAll")){
            this.consoleManager.input("\nWrite value: ").backInsteadOfExit();
            value = this.consoleManager.getInputResult();
        }

        result.put("command", command);
        result.put("value", value);

        return result;
    }
}
