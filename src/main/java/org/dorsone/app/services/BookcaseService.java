package org.dorsone.app.services;

import org.dorsone.app.entities.Bookcase;
import org.dorsone.app.managers.ConsoleManager;
import org.dorsone.app.validators.ValidateBookcaseCommand;

import java.util.ArrayList;
import java.util.Collections;

public class BookcaseService extends FurnitureService {
    public BookcaseService(ConsoleManager consoleManager) {
        super(consoleManager, Bookcase.getCommands(), new ArrayList<>(Collections.singleton(new ValidateBookcaseCommand())));
    }
}
