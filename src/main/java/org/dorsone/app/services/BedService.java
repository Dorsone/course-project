package org.dorsone.app.services;

import org.dorsone.app.validators.ValidateBedCommand;
import org.dorsone.app.entities.Bed;
import org.dorsone.app.managers.ConsoleManager;

import java.util.ArrayList;
import java.util.Collections;

public class BedService extends FurnitureService {

    public BedService(ConsoleManager consoleManager) {
        super(consoleManager, Bed.getCommands(), new ArrayList<>(Collections.singleton(new ValidateBedCommand())));
    }
}
