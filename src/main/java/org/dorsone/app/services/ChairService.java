package org.dorsone.app.services;

import org.dorsone.app.entities.Chair;
import org.dorsone.app.managers.ConsoleManager;
import org.dorsone.app.validators.ValidateChairCommand;

import java.util.ArrayList;
import java.util.Collections;

public class ChairService extends FurnitureService {
    public ChairService(ConsoleManager consoleManager) {
        super(consoleManager, Chair.getCommands(), new ArrayList<>(Collections.singleton(new ValidateChairCommand())));
    }
}
