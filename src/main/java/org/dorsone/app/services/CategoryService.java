package org.dorsone.app.services;

import org.dorsone.app.constants.FileNames;
import org.dorsone.app.managers.ConsoleManager;

public class CategoryService extends Service{
    public CategoryService(ConsoleManager consoleManager) {
        super(consoleManager);
    }

    public String getListOfCommands()
    {
        StringBuilder builder = new StringBuilder();

        builder.append("\n").append("\u001B[1m").append("Choose category").append("\u001B[0m").append(" (");

        FileNames[] categories = FileNames.values();
        int categoriesCount = categories.length;

        for (int i = 0; i < categoriesCount; i++) {
            if ((categoriesCount - 1) == i) {
                builder.append(categories[i]).append("): ");
            } else {
                builder.append(categories[i]).append(", ");
            }
        }

        return builder.toString();
    }
}
