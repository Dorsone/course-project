package org.dorsone.app.services;

import org.dorsone.app.validators.ValidateCustomInput;
import org.dorsone.app.validators.ValidateIsInteger;
import org.dorsone.app.constants.Materials;
import org.dorsone.app.dto.FurnitureDTO;
import org.dorsone.app.exceptions.ValidationErrorException;
import org.dorsone.app.managers.ConsoleManager;
import org.dorsone.app.validators.ValidateMaterial;
import org.dorsone.app.validators.ValidateRangeDouble;

import java.util.ArrayList;
import java.util.List;

public class SearchService extends Service {

    protected ArrayList<? extends FurnitureDTO> data;

    public SearchService(ArrayList<? extends FurnitureDTO> data, ConsoleManager consoleManager) {
        super(consoleManager);
        this.data = data;
    }

    public List<? extends FurnitureDTO> getBy(String by, String value) throws ValidationErrorException {
        return switch (by) {
            case "materials":
                yield this.getByMaterial(value);
            case "width":
                yield this.getByWidth(value);
            case "price":
                yield this.getByPrice(value);
            case "getAll":
                yield this.data.stream().toList();
            case "custom":
                yield this.getBy(value);
            default:
                throw new ValidationErrorException("Column not found!");
        };
    }

    protected List<? extends FurnitureDTO> getBy(String value) throws ValidationErrorException {
        (new ValidateCustomInput()).validate(value);

        String columnName = value.split("=")[0];
        String valueName = value.split("=")[1];

        if (!data.get(0).data.containsKey(columnName)) {
            throw new ValidationErrorException("Column not found!");
        }

        return data.stream()
                .filter(item -> item.data.get(columnName).equals(valueName))
                .toList();
    }

    protected List<? extends FurnitureDTO> getByMaterial(String value) throws ValidationErrorException {
        (new ValidateMaterial()).validate(value);
        Materials material = Materials.valueOf(value);

        return data.stream()
                .filter(item -> item.material.equals(material))
                .toList();
    }

    protected List<? extends FurnitureDTO> getByWidth(String value) throws ValidationErrorException {
        (new ValidateRangeDouble()).validate(value);
        String[] range = value.split("-");

        return data.stream()
                .filter(item -> item.width >= Double.parseDouble(range[0]) && item.width <= Double.parseDouble(range[1]))
                .toList();
    }

    protected List<? extends FurnitureDTO> getByPrice(String value) throws ValidationErrorException {
        (new ValidateIsInteger()).validate(value);
        int price = Integer.parseInt(value);

        return data.stream()
                .filter(item -> item.price == price)
                .toList();
    }
}
