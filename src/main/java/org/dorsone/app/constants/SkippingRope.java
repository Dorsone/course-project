package org.dorsone.app.constants;

public enum SkippingRope {
    soft, middleSoft, resilient
}