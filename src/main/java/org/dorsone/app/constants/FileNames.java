package org.dorsone.app.constants;

public enum FileNames {
    bed, bookcase, chair, sofa;

    public String getPath()
    {
        return "./src/main/resources/" + name() + ".csv";
    }
}
