package org.dorsone.app.validators;

import org.dorsone.app.constants.FileNames;
import org.dorsone.app.exceptions.ValidationErrorException;

public class ValidateCorrectCategory extends Validator {
    @Override
    public void validate(String data) throws ValidationErrorException {
        try {
            FileNames.valueOf(data);
        } catch (IllegalArgumentException exception) {
            throw new ValidationErrorException("Invalid category name");
        }
    }
}
