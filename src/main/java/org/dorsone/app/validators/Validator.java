package org.dorsone.app.validators;

import org.dorsone.app.exceptions.ValidationErrorException;

public abstract class Validator {
    public abstract void validate(String data) throws ValidationErrorException;
}
