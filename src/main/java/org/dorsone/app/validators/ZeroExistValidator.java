package org.dorsone.app.validators;

import org.dorsone.app.exceptions.ValidationExitException;

public class ZeroExistValidator extends Validator {

    public String data;

    public ZeroExistValidator(String dataForValidate) {
        this.data = dataForValidate;
    }

    public void validate() throws ValidationExitException {
        this.validate(this.data);
    }

    @Override
    public void validate(String data) throws ValidationExitException {
        if (
                data.trim().equals("0")
        ) {
            throw new ValidationExitException("Exiting program...");
        }
    }
}
