package org.dorsone.app.validators;

import org.dorsone.app.exceptions.ValidationErrorException;

public class ValidateIsInteger extends Validator {
    @Override
    public void validate(String data) throws ValidationErrorException {
        try {
            Integer.parseInt(data);
        } catch (Exception exception) {
            throw new ValidationErrorException("Invalid integer");
        }
    }
}
