package org.dorsone.app.validators;

import org.dorsone.app.constants.Materials;
import org.dorsone.app.exceptions.ValidationErrorException;

public class ValidateMaterial extends Validator {
    @Override
    public void validate(String data) throws ValidationErrorException {
        try {
            Materials.valueOf(data);
        } catch (Exception exception) {
            throw new ValidationErrorException("Invalid material name");
        }
    }
}
