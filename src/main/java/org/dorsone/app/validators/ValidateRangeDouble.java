
package org.dorsone.app.validators;

import org.dorsone.app.exceptions.ValidationErrorException;

public class ValidateRangeDouble extends Validator {
    @Override
    public void validate(String data) throws ValidationErrorException {
        try {
            String[] range = data.split("-");
            double from = Double.parseDouble(range[0]);
            double to = Double.parseDouble(range[1]);

            if (from > to) {
                throw new ValidationErrorException(to + " must be greater than " + from);
            }

        } catch (NumberFormatException ignored) {
            throw new ValidationErrorException("Invalid type double range");
        }
    }
}
