package org.dorsone.app.validators;

import org.dorsone.app.exceptions.GoBackException;
import org.dorsone.app.exceptions.ValidationErrorException;

public class ValidateRedirect extends Validator {
    @Override
    public void validate(String data) throws ValidationErrorException {
        if (
                data.trim().equals("0")
        ) {
            throw new GoBackException("category.index");
        }
    }
}
