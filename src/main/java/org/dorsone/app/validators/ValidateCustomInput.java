package org.dorsone.app.validators;

import org.dorsone.app.exceptions.ValidationErrorException;

public class ValidateCustomInput extends Validator {
    @Override
    public void validate(String data) throws ValidationErrorException {
        String[] split = data.split("=");
        if (split.length != 2) {
            throw new ValidationErrorException("Invalid custom input type, It will be customColumn=customValue");
        }
    }
}
