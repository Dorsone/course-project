package org.dorsone.app.validators;

import org.dorsone.app.entities.Bookcase;
import org.dorsone.app.exceptions.SuccessException;
import org.dorsone.app.exceptions.ValidationErrorException;

public class ValidateBookcaseCommand extends Validator {
    @Override
    public void validate(String data) throws ValidationErrorException {
        try {
            for (String s : Bookcase.getCommands()) {
                if (s.equals(data)) {
                    throw new SuccessException("success");
                }
            }
            throw new ValidationErrorException("Command " + data + " not found");
        } catch (SuccessException ignored) {
        }
    }
}
