package org.dorsone.app.controllers;

import org.dorsone.app.exceptions.GoBackException;
import org.dorsone.app.exceptions.RouteNotFoundException;
import org.dorsone.app.exceptions.ValidationExitException;
import org.dorsone.app.interfaces.Executable;
import org.dorsone.app.managers.ConsoleManager;
import org.dorsone.routes.Router;

public abstract class Controller implements Runnable {
    protected final ConsoleManager manager;
    protected final Router router;

    public Controller(Router router, ConsoleManager manager) {
        this.router = router;
        this.manager = manager;
    }

    public void execute(Executable executable) {
        while (true) {
            try {
                executable.execute();
                break;
            } catch (ValidationExitException exception) {
                manager.println("\n" + ConsoleManager.ANSI_YELLOW + exception.getMessage() + ConsoleManager.ANSI_RESET);
                System.exit(0);
            } catch (GoBackException exception) {
                try {
                    this.manager.clearValidations();
                    this.router.redirectTo(exception.getMessage());
                } catch (RouteNotFoundException e) {
                    manager.println("\n" + ConsoleManager.ANSI_RED + exception.getMessage() + ConsoleManager.ANSI_RESET);
                }
            } catch (Exception exception) {
                manager.println("\n" + ConsoleManager.ANSI_RED + exception.getMessage() + ConsoleManager.ANSI_RESET);
            }
        }
    }

    @Override
    public void run() {
    }
}
