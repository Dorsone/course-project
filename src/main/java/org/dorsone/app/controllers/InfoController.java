package org.dorsone.app.controllers;

import org.dorsone.app.managers.ConsoleManager;
import org.dorsone.routes.Router;

public class InfoController extends Controller {

    public InfoController(Router router, ConsoleManager manager) {
        super(router, manager);
    }

    public void index() {
        manager.println("Furniture Warehouse (v0.0.1 16.05.2023)")
                .println("Jasur Dustmurodov (jasurbek_dusmurodov@student.itpu.uz)")
                .println("For exit or go back program print \"0\" from every where");
    }

}
