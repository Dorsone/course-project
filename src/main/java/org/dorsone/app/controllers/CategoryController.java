package org.dorsone.app.controllers;

import org.dorsone.app.interfaces.Executable;
import org.dorsone.app.managers.ConsoleManager;
import org.dorsone.app.services.CategoryService;
import org.dorsone.routes.Router;
import org.dorsone.app.validators.ValidateCorrectCategory;

public class CategoryController extends Controller {

    protected CategoryService service;

    public CategoryController(Router router, ConsoleManager manager, CategoryService service) {
        super(router, manager);
        this.service = service;
    }

    public void index()
    {
        Executable executable = () -> {
            manager.input(this.service.getListOfCommands()).addValidator(new ValidateCorrectCategory());
            this.router.redirectTo(manager.getInputResult() + ".index");
        };

        this.execute(executable);
    }
}
