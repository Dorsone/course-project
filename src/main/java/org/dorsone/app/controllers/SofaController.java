package org.dorsone.app.controllers;

import org.dorsone.app.entities.Sofa;
import org.dorsone.app.managers.ConsoleManager;
import org.dorsone.app.services.SofaService;
import org.dorsone.routes.Router;

public class SofaController extends FurnitureController {

    public SofaController(Router router, ConsoleManager manager, SofaService service) {
        super(router, manager, service);

        this.parsedData = Sofa.parseToArrayList();
        this.redirectTo = "sofa.index";
    }

}
