package org.dorsone.app.controllers;

import org.dorsone.app.entities.Bookcase;
import org.dorsone.app.managers.ConsoleManager;
import org.dorsone.app.services.BookcaseService;
import org.dorsone.routes.Router;

public class BookcaseController extends FurnitureController {

    public BookcaseController(Router router, ConsoleManager manager, BookcaseService service) {
        super(router, manager, service);

        this.parsedData = Bookcase.parseToArrayList();
        this.redirectTo = "bookcase.index";
    }
}
