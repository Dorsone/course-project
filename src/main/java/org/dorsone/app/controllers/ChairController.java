package org.dorsone.app.controllers;

import org.dorsone.app.entities.Chair;
import org.dorsone.app.managers.ConsoleManager;
import org.dorsone.app.services.ChairService;
import org.dorsone.routes.Router;

public class ChairController extends FurnitureController {

    public ChairController(Router router, ConsoleManager manager, ChairService service) {
        super(router, manager, service);

        this.parsedData = Chair.parseToArrayList();
        this.redirectTo = "chair.index";
    }
}
