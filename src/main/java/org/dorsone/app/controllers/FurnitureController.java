package org.dorsone.app.controllers;

import org.dorsone.app.dto.FurnitureDTO;
import org.dorsone.app.interfaces.Executable;
import org.dorsone.app.managers.ConsoleManager;
import org.dorsone.app.services.FurnitureService;
import org.dorsone.app.services.SearchService;
import org.dorsone.routes.Router;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class FurnitureController extends Controller {
    protected final FurnitureService service;
    protected String redirectTo;
    protected ArrayList<? extends FurnitureDTO> parsedData;

    public FurnitureController(Router router, ConsoleManager manager, FurnitureService service) {
        super(router, manager);
        this.service = service;
    }

    public void index() {
        SearchService searchService = new SearchService(this.parsedData, manager);

        Executable executable = () -> {
            HashMap<String, String> commandValue = this.service.getCommandValue();
            List<? extends FurnitureDTO> result = searchService.getBy(commandValue.get("command"), commandValue.get("value"));

            this.manager.println(result.toString());
            this.router.redirectTo(this.redirectTo);
        };

        this.execute(executable);
    }

}
