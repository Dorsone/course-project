package org.dorsone.app.controllers;

import org.dorsone.app.entities.Bed;
import org.dorsone.app.managers.ConsoleManager;
import org.dorsone.routes.Router;
import org.dorsone.app.services.BedService;

public class BedController extends FurnitureController {

    public BedController(Router router, ConsoleManager manager, BedService service) {
        super(router, manager, service);

        this.parsedData = Bed.parseToArrayList();
        this.redirectTo = "bed.index";
    }
}
