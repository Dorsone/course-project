package org.dorsone.app.exceptions;

public class SuccessException extends Exception{
    public SuccessException(String message) {
        super(message);
    }
}
