package org.dorsone.app.exceptions;

public class ValidationErrorException extends Exception{
    public ValidationErrorException(String message) {
        super(message);
    }
}
