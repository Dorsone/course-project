package org.dorsone.app.exceptions;

public class RouteNotFoundException extends ValidationErrorException {
    public RouteNotFoundException(String message) {
        super(message);
    }
}
