package org.dorsone.app.exceptions;

public class ValidationExitException extends ValidationErrorException{
    public ValidationExitException(String message) {
        super(message);
    }
}
