package org.dorsone.app.exceptions;

public class GoBackException extends ValidationErrorException{
    public GoBackException(String message) {
        super(message);
    }
}
