package org.dorsone.app.managers;

import org.dorsone.app.exceptions.ValidationErrorException;
import org.dorsone.app.validators.ValidateRedirect;
import org.dorsone.app.validators.Validator;
import org.dorsone.app.validators.ZeroExistValidator;

import java.util.ArrayList;
import java.util.Scanner;

public class ConsoleManager {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_YELLOW = "\u001B[33m";

    private ArrayList<Validator> validators;
    private final Scanner scanner;
    private String inputResult;

    public ConsoleManager() {
        this.validators = new ArrayList<>();
        this.scanner = new Scanner(System.in);
    }

    public ConsoleManager input(String message) {
        this.print(message);
        this.inputResult = scanner.next();
        this.addValidator(new ZeroExistValidator(this.inputResult));

        return this;
    }

    public void print(String message) {
        System.out.print(message);
    }

    public String getInputResult() throws ValidationErrorException {
        for (Validator validator : this.validators) {
            validator.validate(this.inputResult);
        }

        this.validators = new ArrayList<>();

        return this.inputResult;
    }

    public ConsoleManager println(String message) {
        this.print(message + "\n");

        return this;
    }

    public void addValidator(Validator validator) {
        this.validators.add(validator);
    }

    public ConsoleManager backInsteadOfExit() {
        this.validators.remove(0);
        this.validators.add(new ValidateRedirect());

        return this;
    }

    public void clearValidations() {
        this.validators = new ArrayList<>();
    }

    public void addValidators(ArrayList<Validator> validator) {
        this.validators.addAll(validator);
    }
}
