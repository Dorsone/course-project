package org.dorsone.app.managers;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class CsvManager {

    private static final CSVFormat DEFAULT = CSVFormat.newFormat(';').withRecordSeparator("\r\n").withFirstRecordAsHeader();

    public static ArrayList<Map<String, String>> parse(FileReader file) throws IOException {
        CSVParser csvParsed = new CSVParser(file, DEFAULT);

        Iterator<CSVRecord> iterator = csvParsed.iterator();
        ArrayList<Map<String, String>> result = new ArrayList<>();

        while (iterator.hasNext()) {
            result.add(iterator.next().toMap());
        }

        return result;
    }
}
