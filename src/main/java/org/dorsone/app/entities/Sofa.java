package org.dorsone.app.entities;

import org.dorsone.app.constants.FileNames;
import org.dorsone.app.dto.FurnitureDTO;
import org.dorsone.app.dto.SofaDTO;

import java.util.ArrayList;

public class Sofa extends Furniture {
    public static ArrayList<? extends FurnitureDTO> parseToArrayList() {
        return (new SofaDTO(parseFurniture(FileNames.sofa))).toArrayList();
    }
}
