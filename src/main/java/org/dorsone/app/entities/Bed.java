package org.dorsone.app.entities;

import org.dorsone.app.dto.BedDTO;
import org.dorsone.app.dto.FurnitureDTO;
import org.dorsone.app.constants.FileNames;

import java.util.ArrayList;

public class Bed extends Furniture {

    public static ArrayList<? extends FurnitureDTO> parseToArrayList() {
        return (new BedDTO(parseFurniture(FileNames.bed))).toArrayList();
    }
}
