package org.dorsone.app.entities;

import org.dorsone.app.constants.FileNames;
import org.dorsone.app.dto.ChairDTO;
import org.dorsone.app.dto.FurnitureDTO;

import java.util.ArrayList;

public class Chair extends Furniture {
    public static ArrayList<? extends FurnitureDTO> parseToArrayList() {
        return (new ChairDTO(parseFurniture(FileNames.chair))).toArrayList();
    }
}
