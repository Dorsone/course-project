package org.dorsone.app.entities;

import org.dorsone.app.constants.FileNames;
import org.dorsone.app.dto.BookcaseDTO;
import org.dorsone.app.dto.FurnitureDTO;

import java.util.ArrayList;

public class Bookcase extends Furniture {
    public static ArrayList<? extends FurnitureDTO> parseToArrayList() {
        return (new BookcaseDTO(parseFurniture(FileNames.bookcase))).toArrayList();
    }
}
