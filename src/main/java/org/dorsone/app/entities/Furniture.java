package org.dorsone.app.entities;

import org.dorsone.app.constants.FileNames;
import org.dorsone.app.managers.CsvManager;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;

public abstract class Furniture extends CsvManager {
    public static LinkedList<String> getCommands() {
        LinkedList<String> commands = new LinkedList<>();

        commands.add("materials");
        commands.add("price");
        commands.add("width");
        commands.add("getAll");
        commands.add("custom");

        return commands;
    }

    public static ArrayList<Map<String, String>> parseFurniture(FileNames fileName) {
        try {
            FileReader reader = new FileReader(fileName.getPath());

            return parse(reader);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
