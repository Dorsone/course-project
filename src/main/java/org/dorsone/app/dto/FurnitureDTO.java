package org.dorsone.app.dto;

import org.dorsone.app.constants.Materials;

import java.util.ArrayList;
import java.util.Map;

abstract public class FurnitureDTO extends DTO {

    protected ArrayList<Map<String, String>> arrayListData;

    public long price;
    public double width;
    public double height;
    public double weight;
    public double depth;
    public Materials material;

    public Map<String, String> data;

    public FurnitureDTO(Map<String, String> data) {
        this.price = Long.parseLong(data.get("price"));
        this.width = Double.parseDouble(data.get("width"));
        this.height = Double.parseDouble(data.get("height"));
        this.weight = Double.parseDouble(data.get("weight"));
        this.depth = Double.parseDouble(data.get("depth"));
        this.material = Materials.valueOf(data.get("material"));
        this.data = data;
    }

    public FurnitureDTO(ArrayList<Map<String, String>> data) {
        this.arrayListData = data;
    }

    abstract public ArrayList<? extends FurnitureDTO> toArrayList();

    public String toString() {
        return "\u001B[1m" + getClass().getSimpleName().toUpperCase() + "\u001B[0m \n" +
                "\u001B[1m" + "Price: " + "\u001B[0m" + price + "\n" +
                "\u001B[1m" + "Weight: " + "\u001B[0m" + weight + "\n" +
                "\u001B[1m" + "Width: " + "\u001B[0m" + width + "\n" +
                "\u001B[1m" + "Height: " + "\u001B[0m" + height + "\n" +
                "\u001B[1m" + "Depth: " + "\u001B[0m" + depth + "\n" +
                "\u001B[1m" + "Material: " + "\u001B[0m" + material.toString();
    }
}
