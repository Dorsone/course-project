package org.dorsone.app.dto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class BookcaseDTO extends FurnitureDTO {

    protected int numOfBooks;

    public BookcaseDTO(Map<String, String> data) {
        super(data);
        this.numOfBooks = Integer.parseInt(data.getOrDefault("numOfBooks", "0"));
    }

    public BookcaseDTO(ArrayList<Map<String, String>> data) {
        super(data);
    }

    @Override
    public ArrayList<? extends FurnitureDTO> toArrayList() {
        Iterator<Map<String, String>> iterator = this.arrayListData.iterator();
        ArrayList<BookcaseDTO> results = new ArrayList<>();

        while (iterator.hasNext()) {
            results.add(new BookcaseDTO(iterator.next()));
        }

        return results;
    }

    @Override
    public String toString() {
        return "\n\n" + super.toString() + "\n"
                + "\u001B[1m" + "Num. of books: " + "\u001B[0m" + this.numOfBooks;
    }
}
