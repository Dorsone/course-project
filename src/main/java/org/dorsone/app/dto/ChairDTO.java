package org.dorsone.app.dto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class ChairDTO extends FurnitureDTO {

    public ChairDTO(ArrayList<Map<String, String>> data) {
        super(data);
    }

    public ChairDTO(Map<String, String> data) {
        super(data);
    }

    @Override
    public ArrayList<? extends FurnitureDTO> toArrayList() {
        Iterator<Map<String, String>> iterator = this.arrayListData.iterator();
        ArrayList<ChairDTO> results = new ArrayList<>();

        while (iterator.hasNext()) {
            results.add(new ChairDTO(iterator.next()));
        }

        return results;
    }
}
