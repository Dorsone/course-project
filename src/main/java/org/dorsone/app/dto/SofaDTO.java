package org.dorsone.app.dto;

import org.dorsone.app.constants.SkippingRope;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class SofaDTO extends FurnitureDTO{
    protected SkippingRope skippingRope;

    public SofaDTO(Map<String, String> data) {
        super(data);
        this.skippingRope = SkippingRope.valueOf(data.getOrDefault("skippingRope", "soft"));
    }

    public SofaDTO(ArrayList<Map<String, String>> data) {
        super(data);
    }

    @Override
    public ArrayList<? extends FurnitureDTO> toArrayList() {
        Iterator<Map<String, String>> iterator = this.arrayListData.iterator();
        ArrayList<SofaDTO> results = new ArrayList<>();

        while (iterator.hasNext()) {
            results.add(new SofaDTO(iterator.next()));
        }

        return results;
    }

    @Override
    public String toString() {
        return "\n\n" + super.toString() + "\n"
                + "\u001B[1m" + "Skipping Rope: " + "\u001B[0m" + this.skippingRope + "\n";
    }
}
