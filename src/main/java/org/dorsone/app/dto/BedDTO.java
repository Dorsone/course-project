package org.dorsone.app.dto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class BedDTO extends FurnitureDTO {

    public int numOfPeopleForSleep;

    public BedDTO(Map<String, String> data) {
        super(data);
        this.numOfPeopleForSleep = Integer.parseInt(data.getOrDefault("numOfPeopleForSleep", "0"));
    }

    public BedDTO(ArrayList<Map<String, String>> data) {
        super(data);
    }

    @Override
    public ArrayList<? extends FurnitureDTO> toArrayList() {
        Iterator<Map<String, String>> iterator = this.arrayListData.iterator();
        ArrayList<BedDTO> results = new ArrayList<>();

        while (iterator.hasNext()) {
            results.add(new BedDTO(iterator.next()));
        }

        return results;
    }

    @Override
    public String toString() {
        return "\n\n" + super.toString() + "\n"
                + "\u001B[1m" + "Num. of people for sleep: " + "\u001B[0m" + this.numOfPeopleForSleep;
    }
}
