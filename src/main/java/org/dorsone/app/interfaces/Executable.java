package org.dorsone.app.interfaces;

import org.dorsone.app.exceptions.ValidationErrorException;

public interface Executable {
    void execute() throws ValidationErrorException;
}
