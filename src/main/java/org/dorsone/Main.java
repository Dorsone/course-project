package org.dorsone;

import org.dorsone.routes.Router;
import org.dorsone.routes.RoutesList;

public class Main {
    public static void main(String[] args) {
        Router router = new RoutesList();
        router.startProgram();
    }
}