package org.dorsone.routes;

import org.dorsone.app.controllers.*;
import org.dorsone.app.services.*;

public class RoutesList extends Router {
    protected CategoryController categoryController;
    protected InfoController mainController;
    protected BedController bedController;
    protected BookcaseController bookcaseController;
    protected ChairController chairController;
    protected SofaController sofaController;

    protected void defineControllers() {
        this.categoryController = new CategoryController(this, this.consoleManager, new CategoryService(this.consoleManager));
        this.mainController = new InfoController(this, this.consoleManager);
        this.bedController = new BedController(this, this.consoleManager, new BedService(this.consoleManager));
        this.bookcaseController = new BookcaseController(this, this.consoleManager, new BookcaseService(this.consoleManager));
        this.chairController = new ChairController(this, this.consoleManager, new ChairService(this.consoleManager));
        this.sofaController = new SofaController(this, this.consoleManager, new SofaService(this.consoleManager));
    }

    protected void defineRoutes() {
        this.create("main.index", mainController::index);
        this.create("category.index", categoryController::index);
        this.create("bed.index", bedController::index);
        this.create("bookcase.index", bookcaseController::index);
        this.create("chair.index", chairController::index);
        this.create("sofa.index", sofaController::index);
    }
}
