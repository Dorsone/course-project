package org.dorsone.routes;

import org.dorsone.app.exceptions.RouteNotFoundException;
import org.dorsone.app.managers.ConsoleManager;

import java.util.HashMap;
import java.util.LinkedHashMap;

public abstract class Router {

    protected final ConsoleManager consoleManager;

    abstract void defineControllers();

    abstract void defineRoutes();

    public final void redirectTo(String routeName) throws RouteNotFoundException {
        Runnable method = this.get(routeName);

        if (method != null) {
            method.run();
        } else {
            throw new RouteNotFoundException("Selected route not found!");
        }
    }

    public Router() {
        this.consoleManager = new ConsoleManager();
        this.routes = new LinkedHashMap<>();

        this.defineControllers();
    }

    public final void startProgram() {
        try {
            this.defineRoutes();

            for (Runnable value : this.getRoutes().values()) {
                value.run();
            }

        } catch (Exception e) {
            this.consoleManager.println("\n" + ConsoleManager.ANSI_YELLOW + e.getMessage() + ConsoleManager.ANSI_RESET);
        }
    }

    protected final LinkedHashMap<String, Runnable> routes;

    public final HashMap<String, Runnable> getRoutes() {
        return this.routes;
    }

    public final void create(String methodName, Runnable methodForRun) {
        this.routes.put(methodName, methodForRun);
    }

    public final Runnable get(String name) {
        return this.routes.get(name);
    }
}
