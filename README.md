## ALERT: There are some updates after video presentation

# Furniture Warehouse

## Documentation

### Installation

1) Clone repository

```bash
git clone https://gitlab.com/Dorsone/course-project.git
cd course-project
```

2) Run tests. There are two tests, first one for testing entities and second one is for testing SearchService
3) Now you can run the program

### Available Materials and Skipping Rope for searching:

| Materials | Skipping Rope |
|-----------|:--------------|
| wood      | soft          |
| glass     | middleSoft    |
| plastic   | resilient     |
| marble    |               |
| dsp       |               |

### You can search by custom columns, when you choose the custom searching method. Available custom commands:

| Commands            | Furniture Type |
|---------------------|:---------------|
| price               | all            |
| width               | all            |
| height              | all            |
| depth               | all            |
| weight              | all            |
| material            | all            |
| numOfPeopleForSleep | Bed            |
| numOfBooks          | Bookcase       |
| skippingRope        | Sofa           |

UPD: example of custom searching ```numOfBooks=6```